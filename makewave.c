/* NOTE: This must link to winmm.lib. */

#include <stdio.h>
#include <windows.h>
#include <mmsystem.h>

#define BYTES_PER_SAMPLE 2

void checkOpen(void* file, char* name) {
	if (file == NULL) {
		printf("Unable to open file %s.", name);
		exit(-1);
	}
}
void checkWrite(int bytesRead) {
	if (bytesRead == -1) {
		puts("Error writing to the output file");
		exit(-1);
	}
}
void check(MMRESULT result) {
	if (result != 0 && result != MMSYSERR_NOERROR) {
		TCHAR errText[MAXERRORLENGTH + 1];
		if (waveOutGetErrorText(result, errText, MAXERRORLENGTH) == MMSYSERR_NOERROR) {
			puts(errText);
		} else puts("Error converting output error code to text message.");		
		exit (-1);
	}
}
void showHelp() {
	puts("Makes a mono 44.1kHz 16-bit wave file from a file of enter-separated numbers.");
	puts("Usage: makewave [infile (full name)] [outfile] \n   or: makewave [infile (assumed .txt)] (outfile assumed to be [infile].wav)");
	exit(0);
}
void processArgs(int argc, char *argv[], char** inFile, char** outFile) {
	if (argc < 2) {
		showHelp();
	} else if (argc == 2) {
		if (strcmp(argv[1], "-help") == 0 || strcmp(argv[1], "-h") ==0 ||
			 strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "--h") == 0) {
			showHelp();
		} else {
			int length = strlen(argv[1]);
			*inFile = new char[length + 4];
			*outFile = new char[length + 4];
			sprintf(*inFile, "%s.txt", argv[1]);
			sprintf(*outFile, "%s.wav", argv[1]);
		}
	} else {
		*inFile = argv[1];
		*outFile = argv[2];
	}
}
int countSamplesInFile(FILE* stream) {
	int result;
	int fileSize = 0;
	int datum;
	result = fscanf(stream, "%i\n", &datum);
	while (result != EOF) {
		fileSize++;
		result = fscanf(stream, "%i\n", &datum);
	}
	fseek(stream, 0L, SEEK_SET);
	return fileSize;
}
char* readSamples(FILE* inStream, int bytesOfData) {
	int datum, bytesRead, bufferIndex;

	char* buffer = new char[bytesOfData];
	if (!buffer) {
		puts("Unable to allocate buffer!");
		exit(-1);
	}

	bufferIndex = 0;
	bytesRead = fscanf(inStream, "%i\n", &datum);
	while (bytesRead != EOF) {
		buffer[bufferIndex] = datum;
		buffer[bufferIndex + 1] = datum >> 8;
		bytesRead = fscanf(inStream, "%i\n", &datum);
		bufferIndex += BYTES_PER_SAMPLE;
	}

	return buffer;
}
void writeFormat(HMMIO outStream) {
	MMCKINFO formatChunkInfo = {0};

	/* the others depend on wBitsPerSample, but it stored last in the format.*/
	WORD wBitsPerSample = BYTES_PER_SAMPLE * 8;

	WORD formatTag = 1;
	WORD nChannels = 1;
	DWORD nSamplesPerSec = 44100;
	DWORD nAvgBytesPerSec = nChannels * wBitsPerSample / 8 * nSamplesPerSec;
	WORD nBlockAlign = nChannels * wBitsPerSample / 8;	

	char* format;
	int formatSize = 16;
	format = new char[formatSize];
	format[0] = (char) (formatTag);
	format[1] = (char) (formatTag >> 8);
	format[2] = (char) (nChannels);
	format[3] = (char) (nChannels >> 8);
	format[4] = (char) (nSamplesPerSec);
	format[5] = (char) (nSamplesPerSec >> 8);
	format[6] = (char) (nSamplesPerSec >> 16);
	format[7] = (char) (nSamplesPerSec >> 24);
	format[8] = (char) (nAvgBytesPerSec);
	format[9] = (char) (nAvgBytesPerSec >> 8);
	format[10] = (char) (nAvgBytesPerSec >> 16);
	format[11] = (char) (nAvgBytesPerSec >> 24);
	format[12] = (char) (nBlockAlign);
	format[13] = (char) (nBlockAlign >> 8);
	format[14] = (char) (wBitsPerSample);
	format[15] = (char) (wBitsPerSample >> 8);

	formatChunkInfo.ckid = mmioStringToFOURCC("fmt ", 0);
	formatChunkInfo.cksize = formatSize;
	check(mmioCreateChunk(outStream, &formatChunkInfo, 0));	
	checkWrite(mmioWrite(outStream, format, formatSize));
	check(mmioAscend(outStream, &formatChunkInfo, 0));
}
void makeWaveFile(char* outFile, char* data, int bytesOfData) {
	MMCKINFO riffChuckInfo = {0};	
	MMCKINFO dataChunkInfo = {0};
	HMMIO outStream;

	outStream = mmioOpen(outFile, 0, MMIO_CREATE | MMIO_WRITE);
	checkOpen(outStream, outFile);

	riffChuckInfo.fccType = mmioStringToFOURCC("WAVE", 0);
	check(mmioCreateChunk(outStream, &riffChuckInfo, MMIO_CREATERIFF));

	writeFormat(outStream);

	dataChunkInfo.cksize = bytesOfData;
	dataChunkInfo.ckid = mmioStringToFOURCC("data", 0);
	check(mmioCreateChunk(outStream, &dataChunkInfo, 0));

	checkWrite(mmioWrite(outStream, data, bytesOfData));
	check(mmioAscend(outStream, &dataChunkInfo, 0));

	check(mmioAscend(outStream, &riffChuckInfo, 0));
	check(mmioClose(outStream, 0));
}

void main(int argc, char *argv[], char *envp[]) {	
	char* outFile;
	char* inFile;
	FILE* inStream;	
	char* buffer;
	int bytesOfData;	

	processArgs(argc, argv, &inFile, &outFile);

	inStream = fopen(inFile, "r");
	checkOpen(inStream, inFile);

	printf("Reading data from %s ...\n", inFile);
	bytesOfData = countSamplesInFile(inStream) * BYTES_PER_SAMPLE;
	buffer = readSamples(inStream, bytesOfData);

	makeWaveFile(outFile, buffer, bytesOfData);

	fclose(inStream);

	printf("Successfully created %s.", outFile);
}
